# README

## So what's EXTRA ?

* Per-App Network Isolation
* Sync Tile in QS
* Dynamic Network Traffic Indicator
* Estimated Battery-Life in QS Header
* DeepSleep Info added Uptime info
* Volume Rocker Wake
* QS Header pressing date -> calendar
* Battery Temperature on Power Summary
* Notification Log Exposed
* Added Google Modules (for Google Play System Updates)
* AOSP December 5 Patches Merged
